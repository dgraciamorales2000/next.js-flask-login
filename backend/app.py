from flask import Flask
from dotenv import load_dotenv
import os

from extensions import db

from blueprints.users import users

# Init app
app = Flask(__name__)
load_dotenv()

# Database
app.config['SECRET_KEY'] = os.getenv("SECRET_KEY")
app.config['SQLALCHEMY_DATABASE_URI'] = os.getenv("DATABASE_URI")
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

# Init db
db.init_app(app)
app.register_blueprint(users, url_prefix="/users")

with app.app_context():
  db.create_all()

# Run Server
if __name__ == '__main__':
  app.run(debug=True)
