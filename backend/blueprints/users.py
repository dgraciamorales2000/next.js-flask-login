from flask import Blueprint, jsonify, request, make_response, current_app
from werkzeug.security import generate_password_hash, check_password_hash
import jwt
from functools import wraps

from extensions import db
from models.users import Users

users = Blueprint("users", __name__)

def token_required(f):
  @wraps(f)
  def decorated(*args, **kwargs):
    token = None

    if 'x-access-token' in request.headers:
      token = request.headers['x-access-token']

      if not token:
        return jsonify({ "message" : "Token is missing" }), 401

      try:
        data = jwt.decode(token, current_app.secret_key, algorithms=["HS256"])
        current_user = Users.query.filter_by(username=data['username']).first()
      except:
        return jsonify({ "message" : "Token is invalid" }), 401
      
      return f(current_user, *args, **kwargs)
    
    else:
      return jsonify({ "message" : "Token is missing" }), 401

  return decorated

@users.route('/', methods=['GET'])
def get_users():
  users = Users.query.all()

  output = []

  for user in users:
    user_data = {}
    user_data['id'] = user.id
    user_data['username'] = user.username
    user_data['email'] = user.email
    user_data['admin'] = user.admin

    output.append(user_data)

  return jsonify({ 'users' : output })

@users.route('/<id>', methods=['GET'])
def get_user_by_id(id):
  user = Users.query.filter_by(id=id).first()

  if not user:
    return jsonify({ "message" : "User not found" })

  user_data = {}
  user_data['id'] = user.id
  user_data['username'] = user.username
  user_data['email'] = user.email
  user_data['admin'] = user.admin

  return jsonify({ 'user' : user_data })
    
@users.route('/', methods=['POST'])
def create_user():
  data = request.get_json()

  hashed_password = generate_password_hash(data['password'], method='sha256')

  new_user = Users(username=data['username'], password=hashed_password, email=data['email'], admin=False)
  db.session.add(new_user)
  db.session.commit()

  return jsonify({ "message" : "New user created" })

@users.route('/<id>', methods=['PUT'])
@token_required
def promote_user(current_user, id):
  if not current_user.admin:
    return jsonify({ "message" : "You are not an admin" })

  user = Users.query.filter_by(id=id).first()

  if not user:
    return jsonify({ "message" : "User not found" })

  user.admin = True
  db.session.commit()

  return jsonify({ "message" : "User has been promoted" })

@users.route('/<id>', methods=['DELETE'])
@token_required
def delete_user(current_user, id):
  if not current_user.admin:
    return jsonify({ "message" : "You are not an admin" })

  user = Users.query.filter_by(id=id).first()

  if not user:
    return jsonify({ "message" : "User not found" })
  
  db.session.delete(user)
  db.session.commit()

  return jsonify({ "message" : "User deleted" })
  
@users.route('/login')
def login():
  auth = request.authorization

  if not auth or not auth.username or not auth.password:
    return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

  user = Users.query.filter_by(username=auth.username).first()

  if not user:
    return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

  if check_password_hash(user.password, auth.password):
    token = jwt.encode({ "username" : user.username }, current_app.secret_key)

    return jsonify({ "token" : token })

  return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})
